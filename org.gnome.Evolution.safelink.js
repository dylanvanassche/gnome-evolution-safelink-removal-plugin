/*
 * GNOME Evolution plugin to remove Outlook safelinks in preview
 * Licensed under GNU General Public License v3.0 or later
 * Copyright (c) Dylan Van Assche (2022)
 * Based upon https://wiki.gnome.org/Apps/Evolution/Extensions
 * Inspired by https://addons.thunderbird.net/en-US/thunderbird/addon/safelink-removal/
 */

'use strict';

var orgGnomeEvolutionSafelinkPlugin = {
        name : "orgGnomeEvolutionSafelinkPlugin",
        setup : function(doc) {
                var anchors, ii;
                anchors = doc.getElementsByTagName("A");
                for (ii = 0; ii < anchors.length; ii++) {
                        var url = new URL(anchors[ii]);
                        if (url.searchParams.get('url')) {
                            anchors[ii].href = decodeURIComponent(url.searchParams.get('url'));
                        }
                }
        }
};

Evo.RegisterPlugin(orgGnomeEvolutionSafelinkPlugin);

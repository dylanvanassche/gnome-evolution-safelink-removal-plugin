# GNOME Evolution safelink removal plugin

Tired of these Outlook safelinks in your e-mails?
Remove them with this plugin in GNOME Evolution!

## Installation

- `mkdir ~/.local/share/evolution/preview-plugins/`
- `cp org.gnome.Evolution.safelink.js ~/.local/share/evolution/preview-plugins/org.gnome.Evolution.safelink.js` 
- Restart Evolution

## License

Licensed under the GNU General Public License v3.0 or later.
Copyright (c) Dylan Van Assche (2022)
